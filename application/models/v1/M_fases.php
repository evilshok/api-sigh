<?php

class M_fases extends CI_Model
{
	function get_id_fase($fase){
		$fase = $this->db
			->where('fase',$fase)
			->get('cat_fases')
			->row()->id;
			return $fase;
	}
	function obtener_casos_por_fase($condiciones){
		$this->db->select("casos.*,concat_ws(' ',clientes.nombre,clientes.apellido_paterno, clientes.apellido_materno) as cliente");
		$this->db->from('casos');
		$this->db->join('cliente_asesor', 'casos.id_cliente_asesor = cliente_asesor.id');
		$this->db->join('clientes','cliente_asesor.id_cliente = clientes.id');
		$this->db->where($condiciones);
		$this->db->where('casos.activo',1);
		
		$query = $this->db->get();
		return $query->result_array();
	}
	function actualizar_caso($id_caso,$array){
		$this->db->where('id',$id_caso);
		return $this->db->update('casos',$array);
		// echo die(json_encode($this->db->error()));
	}

	function actualizar_fases($id_caso,$array){
		$this->db->where('id_caso',$id_caso);
		return $this->db->update('fecha_fases',$array);
	}
	function cantidad_etapas($id_fase){
		$existe = $this->db
            ->from('casos')
            ->where('id_fase', $id_fase)
            ->count_all_results();

        return  $existe ;
	}
}