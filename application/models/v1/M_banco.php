<?php

class M_banco extends CI_Model{

    function existe($nombre){
        $existe = $this->db
            ->from('bancos')
            ->where('nombre_banco', strtoupper($nombre))
            ->count_all_results();

        return  $existe > 0;
    }

    function create($nombre){
        $banco = [
            "nombre_banco" => strtoupper($nombre)
        ];

        $insert = $this->db->insert('bancos', $banco);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
	}
	
	function getAll(){
		$banco = $this->db
            ->from('bancos')
            ->get()
            ->result_array();
        return $banco;
	}
	function getApoyos(){
		$apoyos = $this->db
			->from('gobierno')
			->get()
            ->result_array();
        return $apoyos;
	}
}

?>
