<?php

class M_constructora extends CI_Model{

    function existe($nombre)
    {
        $existe = $this->db
            ->from('constructoras')
            ->where('nombre_constructora', strtoupper($nombre))
            ->count_all_results();

        return  $existe > 0;
    }

    function create($nombre)
    {
        $constructora = [
            "nombre_constructora" => strtoupper($nombre),
        ];

        $insert = $this->db->insert('constructoras', $constructora);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('constructoras');
        $this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return null;
        }
    }
    function getAll(){
        $this->db->select('*');
        $this->db->from('constructoras');
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

}
