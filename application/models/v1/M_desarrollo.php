<?php

class M_desarrollo extends CI_Model{

    function existe($nombre)
    {
        $existe = $this->db
            ->from('cat_desarrollo')
            ->where('nombre_desarrollos', strtoupper($nombre))
            ->count_all_results();

        return  $existe > 0;
    }

    function create($nombre,$id_constructora)
    {
        $desarrollo = [
            "nombre_desarrollo" => strtoupper($nombre),
            "id_constructora" => $id_constructora
        ];

        $insert = $this->db->insert('cat_desarrollos', $desarrollo);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('cat_desarrollos');
        $this->db->where('id_desarrollo', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return null;
        }
	}
	
    function getByContructora($id){
        $this->db->select('*');
        $this->db->from('cat_desarrollos');
        $this->db->where('id_constructora', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return null;
        }
    }

}

?>
