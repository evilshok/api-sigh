<?php

class M_inmobiliaria extends CI_Model{

    function existe($nombre)
    {
        $existe = $this->db
            ->from('inmobiliarias')
            ->where('nombre', strtoupper($nombre))
            ->count_all_results();

        return  $existe > 0;
    }

    function create($nombre)
    {
        $inmobiliaria = [
            "nombre" => strtoupper($nombre)
        ];

        $insert = $this->db->insert('inmobiliarias', $inmobiliaria);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
    }

    function get($id){
        $this->db->select('*');
        $this->db->from('inmobiliarias');
        $this->db->where('id_inmobiliaria', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return null;
        }
    }
}

?>