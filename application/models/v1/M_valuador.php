<?php

class M_valuador extends CI_Model{

    function existe($nombre)
    {
        $existe = $this->db
            ->from('valuador')
            ->where('nombre', $nombre)
            ->count_all_results();

        return  $existe > 0;
    }

    function create($nombre,$correo,$telefono)
    {
        $valuador = [
            "nombre" => $nombre,
            "correo" => $correo,
            "telefono" => $telefono
        ];

        $insert = $this->db->insert('valuador', $valuador);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
    }
    function get_all(){
        $this->db->select('*');
        $this->db->from('valuador');
        $query = $this->db->get();

        return $query->result_array();
    }
}

?>