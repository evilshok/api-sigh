<?php
include_once('M_base.php');
class M_bitacora extends M_base{

	function insert_bitacora($id_caso, $operacion, $fecha){
		$new = [
			'id_caso' => $id_caso,
			'operacion' => $operacion,
			'fecha' => $fecha 
		];
		$this->insert('bitacora_casos',$new);
	}
}
?>
