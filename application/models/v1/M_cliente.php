<?php
include_once('M_base.php');
class M_cliente extends M_base{
    
    public function __construct(){
        parent::__construct();
	}

    function existe($curp){
        $existe = $this->db
            ->from('clientes')
            ->where('curp', strtoupper($curp))
            ->count_all_results();

        return  $existe > 0;
    }

    function existe_id($id){
        $existe = $this->db
            ->from('clientes')
            ->where('id', $id)
            ->count_all_results();

        return  $existe > 0;
    }

    function create($id_asesor, $curp, $ap_paterno, $ap_materno, $nombres, $fecha_nacimiento, $correo, $telefono, $ingresos, $tipo_economina){
        $cliente = [
            "curp" => $curp,
            "apellido_paterno" => $ap_paterno,
            "apellido_materno" => $ap_materno,
            "nombre" => $nombres,
            "fecha_nacimiento" => $fecha_nacimiento,
            "correo" => $correo,
            "telefono" => $telefono,
            "ingreso_mensual" => $ingresos,
            "tipo_economia" => $tipo_economina,
        ];

        $insert = $this->db->insert('clientes', $cliente);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();
		
		$id_c_a = $this->crear_relacion_cliente_asesor($id_asesor,$id);

        return $id_c_a;
    }

    function add_esquema_compra($id_cliente_asesor,$id_constructora,$id_desarrollo,$id_inmobiliaria,$valor_vivienda){ 
        $esquema_compra = [
            "id_cliente_asesor" => $id_cliente_asesor,
            "id_constructora" => $id_constructora,
            "id_desarrollo" => $id_desarrollo,
            "id_inmobiliaria" => $id_inmobiliaria,
            "valor_vivienda" => $valor_vivienda,
        ];

        $insert = $this->db->insert('esquema_compra', $esquema_compra);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return $id;
    }

    function get($id){
        $this->db->select('clientes.*, cliente_asesor.id as id_cliente_asesor');
        $this->db->from('clientes');
        $this->db->join('cliente_asesor', 'clientes.id = cliente_asesor.id_cliente');
        $this->db->where('clientes.id', $id);//TODO: insertar el id del usuario en sesion
        $query = $this->db->get();
        // echo die(json_encode($this->db->error()));
        return $query->row_array();
	}

	function crear_relacion_cliente_asesor($id_asesor , $id_cliente){
		
		$create = [
			'id_asesor' => $id_asesor,
			'id_cliente' => $id_cliente
		];
		
		$insert = $this->db->insert('cliente_asesor', $create);
		
		if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();
		
		return $id;
	}

    function get_all(){
        $this->db->select("*, concat_ws(' ', apellido_paterno, apellido_materno, nombre) as nombre_completo");
        $this->db->from('clientes');
        $query = $this->db->get();
        
        return $query->result_array();
    }
}
