<?php

class M_notaria extends CI_Model{

    function existe($nombre)
    {
        $existe = $this->db
            ->from('notarias')
            ->where('nombre_notaria', strtoupper($nombre))
            ->count_all_results();

        return  $existe > 0;
    }

    function create($numero,$nombre,$ciudad)
    {
        $notaria = [
            "notaria" => $numero,
            "nombre_notaria" => strtoupper($nombre),
            "ciudad_notaria" => $ciudad
        ];

        $insert = $this->db->insert('notarias', $notaria);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
    }
    function get_all(){
        $this->db->select('*');
        $this->db->from('notarias');
        $query = $this->db->get();

        return $query->result_array();
    }
}

?>