<?php

class M_asesor extends CI_Model
{

    function exists($correo){
        $existe = $this->db
            ->from('asesor')
            ->where('correo', $correo)
            ->count_all_results();

        return  $existe > 0;
    }

    function existe_id($id){
        $existe = $this->db
            ->from('asesores')
            ->where('id_asesor', $id)
            ->count_all_results();

        return  $existe > 0;
    }

    function create($array){
        $insert = $this->db->insert('asesor',$array);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return $id;
    }


    function update($nombre, $correo, $telefono, $nivel, $permiso_especial, $id_oficina, $id_senior, $id_asesor){
        $this->db->trans_start();
        $success = $this->db->query("call p_alta_nuevo_asesor('$nombre','$correo','$telefono','$nivel','$id_senior','$permiso_especial','$id_asesor','$id_oficina',@respuesta)");
        $success->next_result();
        $success->free_result();
        $query = $this->db->query('select @respuesta as out_param');
        $this->db->trans_complete();

        return $query->row()->out_param;
    }

    function all(){
        $this->db->select('asesor.*');
        $this->db->from('asesor');
        $query = $this->db->get();
        return $query->result_array();
    }

    function get($id){
        $this->db->select('asesor.*');
        $this->db->from('asesor');
        $this->db->where('id', $id);
        $query = $this->db->get();

        return $query->row_array();
    }

    function asesores_mesa_control(){
        $this->db->select('nombre,correo');
        $this->db->from('asesor');
		$this->db->where('tipo_usuario',8);
        $query = $this->db->get();

        return $query->result_array();
    }
    /** Método para obtener el asesor mediante correo y contraseña
     *  @param string $correo Correo del asesor
     *  @param string $clave Contraseña del asesor
     *  @return array|null Si se obtuvo el asesor, se mostrara la información del asesor, sino regresara null
     */
    function get_con_correo_clave(string $correo, string $clave) : array {
        $asesor = $this->db
            ->where('correo', $correo)
            ->where('password', $clave)
            ->get('asesores')
            ->row_array();
        
        return $asesor;
    }
}
