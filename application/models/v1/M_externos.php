<?php

class M_externos extends CI_Model
{

    function existe($correo){
        $existe = $this->db
            ->from('asesores_externos')
            ->where('correo_externo', $correo)
            ->count_all_results();

        return  $existe > 0;
    }

    function create($nombre, $correo, $id_banco, $id_constructora, $id_desarrollo, $id_inmobiliaria){ 
        $externo = $this->map_externo($nombre, $correo, $id_banco, $id_constructora, $id_desarrollo, $id_inmobiliaria);

        $insert = $this->db->insert('asesores_externos', $externo);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');

    }

    function insert_externo($id){
        $asesor_externo = [
            "id_externo" => $id
        ];

        $insert = $this->db->insert('asesores', $asesor_externo);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
    }

    function map_externo($nombre, $correo, $id_banco, $id_constructora, $id_desarrollo, $id_inmobiliaria){
        $array = [];
        if ($id_banco > 0) {
            $array = [
                "asesor_externo" => $nombre,
                "correo_externo" => $correo,
                "id_banco" => $id_banco,
                "id_constructora" => 0,
                "id_desarrollo" => 0,
                "id_inmobiliaria" => 0,
                "permisos" => 256
            ];

            return $array;
        } else if ($id_constructora > 0) {
            if ($id_desarrollo > 0) {
                $array = [
                    "asesor_externo" => $nombre,
                    "correo_externo" => $correo,
                    "id_constructora" => $id_constructora,
                    "id_desarrollo" => $id_desarrollo,
                    "id_inmobiliaria" => 0,
                    "id_banco" => 0,
                    "permisos" => 64
                ];
                return $array;
            } else if ($id_desarrollo == 0) {
                $array = [
                    "asesor_externo" => $nombre,
                    "correo_externo" => $correo,
                    "id_constructora" => $id_constructora,
                    "id_desarrollo" => 0,
                    "id_inmobiliaria" => 0,
                    "id_banco" => 0,
                    "permisos" => 32
                ];

                return $array;
            }
        } else if ($id_inmobiliaria > 0) {
            $array = [
                "asesor_externo" => $nombre,
                "correo_externo" => $correo,
                "id_inmobiliaria" => $id_inmobiliaria,
                "id_banco" => $id_banco,
                "id_constructora" => 0,
                "id_desarrollo" => 0,
                "permisos" => 512
            ];

            return $array;
        }
    }
}
