<?php
include_once('M_base.php');
class M_comentarios extends M_base{

	function create($id_caso,$comentario,$id_fase,$id_usuario,$usuario){
		
		$nuevo_comentario = [
			'id_usuario' => $id_usuario,
			'id_fase' => $id_fase,
			'comentario' => $comentario,
			'id_caso' => $id_caso,
			'usuario' => $usuario,
		];

		return $this->insert('comentarios',$nuevo_comentario);

		// echo die(json_encode($this->db->error()));
	}

	function eliminar_comentario($id_caso){
		$this->db->where('id_caso',$id_caso);
		return $this->db->update('comentarios_fases',array('estatus' => 0));
		//estatus en 0 significa que el el comentario no se mostrara en la interface
	}
	
	function obtener_comentarios($id_caso){
		$this->db->select('*');
        $this->db->from('comentarios');
        $this->db->where('id_caso', $id_caso);
        $query = $this->db->get();

        return $query->result_array();
	}

}

?>
