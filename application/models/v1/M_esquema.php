<?php

class M_esquema extends CI_Model{

    function existe($nombre)
    {
        $existe = $this->db
            ->from('cat_esquemas')
            ->where('nombre_esquema', strtoupper($nombre))
            ->count_all_results();

        return  $existe > 0;
    }

    function create($nombre,$id_banco)
    {
        $esquema = [
            "nombre_esquema" => strtoupper($nombre),
            "id_banco" => $id_banco
        ];

        $insert = $this->db->insert('cat_esquemas', $esquema);

        if (!$insert) {
            return null;
        }

        $id = $this->db->insert_id();

        return compact('id');
	}
	function lista_esquemas($id_banco){

		$esquema = $this->db
			->where('id_banco', $id_banco)
			->get('esquemas')
			->result_array();
        return $esquema;
	}
}

?>
