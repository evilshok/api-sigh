<?php
include_once('M_base.php');
class M_expediente extends M_base{
	//buscar expediente por ID
	function exists($id){
		$existe = $this->db
            ->from('casos')
            ->where('id', $id)
            ->count_all_results();

        return  $existe > 0;
	}
	function existen_expedientes_cliente($id_cliente_asesor){
		$existe = $this->db
		->from('casos')
		->where('id_cliente_asesor', $id_cliente_asesor)
		->count_all_results();

	return  $existe > 0;
	}
    function create($id_usuario,$id_cliente_asesor,$id_apoyo,$id_banco,$id_esquema,$fecha_ingreso_banco,$tasa,$monto_solicitado,$ejecutivo_banco,$plazo,$factor_pago,$comision_apertura,$pago_adelantado,$folio_banco,$folio_soc,$incremento_factor_pago){
        $new = [
			'id_usuario' => $id_usuario,
			'id_cliente_asesor' => $id_cliente_asesor,
			'id_apoyo' => $id_apoyo,
			'id_banco' => $id_banco,
			'id_esquema' => $id_esquema,
			'fecha_ingreso_banco' => $fecha_ingreso_banco,
			'tasa' => $tasa,
			'monto_solicitado' => $monto_solicitado,
			'ejecutivo_banco' => $ejecutivo_banco,
			'plazo' => $plazo,
			'factor_pago' => $factor_pago,
			'comision_apertura' => $comision_apertura,
			'pago_adelantado' => $pago_adelantado,
			'folio_banco' => $folio_banco,
			'folio_soc' => $folio_soc,
			'incremento_factor_pago' => $incremento_factor_pago,
			'id_fase' => 1, //se asigna a la etapa Analisis
			'fecha_ultima_fase' => date('Y/m/d')
		];
		$id = $this->insert('casos',$new);
		// echo die(json_encode($this->db->error()));
		if( $id === null){
			return null;
		}

		$this->insertar_analisis($id,$fecha_ingreso_banco);

		return $id;

    }
	
    function getAll($params){
        $this->db->select('*');
		$this->db->from('v_casos_cliente');
		$conditions = $this->conditions($params);
		if(count($conditions) > 0){
			$this->db->where($conditions);
		}
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->result_array();
        } else {
            return [];
        }
	}
	
    function get($id){
        $this->db->select('*');
		$this->db->from('v_expediente');
		$this->db->where('id', $id);
        $query = $this->db->get();

		return $query->row_array();
    }
    function actualizarTasa($id_caso, $tasa){
		$data = array("tasa" => $tasa);
		$this->db->where("id_caso",$id_caso);
		return $this->db->update("casos",$data);
	}
	function insertar_analisis($id_caso,$fecha_analisis){
		$insertar = [
			"id_caso" => $id_caso,
			"analisis" => $fecha_analisis
		];
		$insert = $this->insert('fecha_fases',$insertar);

		return $insert;
	}
	function exists_folio_soc($id){
		$this->db->select('folio_soc');
		$this->db->from('casos');
		$this->db->where('id', $id);
        $query = $this->db->get();
        if ($query->num_rows() > 0) {
            return $query->row_array();
        } else {
            return [];
        }
	}
	function obtener_datos_planos_expediente($id){
		$this->db->select('*');
        $this->db->from('casos');
		$this->db->where('id',$id);
        $query = $this->db->get();

        return $query->row_array();
	}
	function expedientes_activos($id_cliente_asesor){
		$this->db->select('*');
        $this->db->from('v_expediente');
		$this->db->where('id_cliente_asesor',$id_cliente_asesor);
		$this->db->where('activo',1);
        $query = $this->db->get();

		return $query->result_array();
	}
	function expedientes_no_activos($id_cliente_asesor){
		$this->db->select('*');
        $this->db->from('v_expediente');
		$this->db->where('id_cliente_asesor',$id_cliente_asesor);
		$this->db->where('activo',0);
        $query = $this->db->get();

		return $query->result_array();
	}
	function cancelar_caso($id,$razon){
		$this->db->where("id",$id);
		return $this->db->update("casos",[
			"activo" => 0,
			"razon" => $razon
		]);
	}
	function asesor_expediente($id){
		$this->db->select('asesor.nombre, asesor.correo');
        $this->db->from('casos');
        $this->db->join('cliente_asesor', 'casos.id_cliente_asesor = cliente_asesor.id');
		$this->db->join('asesor','cliente_asesor.id_asesor = asesor.id');
		$this->db->where('casos.id',$id);
		$this->db->where('asesor.estatus',1);
        $query = $this->db->get();

        return $query->row_array();
	}
}
