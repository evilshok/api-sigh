<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Recursos extends REST_Controller{
	protected $permiso = 1;
    function __construct(){
        parent::__construct();

        header('Access-Control-Allow-Origin: '.getenv('ALLOW_ORIGINS'));
        header('Access-Control-Allow-Headers: *');
        header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE, OPTIONS');
        header('Content-Type: application/json');
        
        if(!$this->input->is_ajax_request()){
            
            $resp = [
                'code' => 'no/avaliable',
                'estatus' => 400,
                'error' => "You can't use this API",
            ];
            
            exit(json_encode($resp));
        }

        $post = file_get_contents("php://input");
        
        $this->request_post = json_decode($post, true);
    }
    function request($index = null, $data = null ) {
        if($index == null) {
            return $this->request_post;
        }

        if ($data == null) {
            $data = $this->request_post;
        }

        if(is_array($index)) {
            $params  = [];

            foreach( $index as $name) {
                $params[$name] = isset($data[$name]) ? $data[$name] : null;
            }
            
            return $params;
        }
        
        if(!isset($data[$index])) {
			return null;
		}

		return $this->request_post[$index];
    }

    function validate($config, $data = null) {
        $this->load->library('form_validation');
        
        if($data == null) {
            $data = is_array($this->request_post) ? $this->request_post : [];
        }

        $this->form_validation
            ->set_data($data)
            ->set_rules($config);

        $validacion = $this->form_validation->run();

        if(!$validacion) {
            http_response_code(400);

            $errors = $this->form_validation->error_array();

            if(sizeof($errors) == 0 ) {
                $errors = 'Envie todos los datos solicitados';
            }

            $resp = [
                'error' => 'Invalidate form',
                'status' => 400,
                'code' => 'bad/post',
                'errors' => $errors,   
            ];
            
            die(json_encode($resp));  
        }
	}
	function validar_acceso_usuario($acceso){
		if(($this->permiso & $acceso) > 0){
			return true;
		}
		return false;
	}
	function map_caso($object){
        $array= [
            "id_caso" => $object['id_caso'],
            "cliente" => $object['cliente'],
            "asesor" => $object['asesor'],
            "fecha" => $object['analisis'],
            "monto_solicitado" => $object['monto_solicitado'],
            "nombre_banco" => $object['banco'],
            "banco" => $object['banco'],
            "nombre_esquema" => $object['esquema'],
            "tasa" => $object['tasa']
        ];

        return $array;
    }
}
class Base extends Recursos {
    protected $request_post;
    protected $uid = 1;

    public function __construct()
    {
        parent::__construct();

        // $this->verifyToken();
        $this->loadDatabase("sigh_db_test");
    }

    public function loadDatabase($db_name){
		$dsn = getenv('DB_CONNECTION').'://'.getenv('DB_USERNAME').':'.getenv('DB_PASSWORD').'@'.getenv('DB_HOST').'/'.$db_name;
		$this->db = $this->load->database($dsn,TRUE);
	}
    
    protected function verifyToken() {
        $this->load->library('jwt');
        
        $token = $this->extractTokenFromAuthHeader();

        $this->uid = $this->jwt->verifyAndGetUid($token);
    }

    /** Method to get the Current User UID
     *  @return string UID of the user
     */
    function getUid() {
        return $this->uid;
    }
    
    protected function extractTokenFromAuthHeader() {
        $headers = getallheaders();
        
        $headers = array_change_key_case($headers, CASE_LOWER);
        
        if(isset($headers['access-control-request-method'])){
            exit(1);
        }
        
        if(!isset($headers["authorization"]) || empty($headers["authorization"])){

            http_response_code(401);

            $resp = [
                'error' => 'Auth needed',
                'status' => 401,
                'code' => 'bad/auth',
                'errors' => 'Tiene que enviar un Token para usar esta API',
            ];
            
            die(json_encode($resp));
        }

        $access_token = explode(" ", $headers["authorization"])[1];

        return $access_token;
    }
    function enviarCorreo($de,$para,$asunto,$mensaje){
		$this->load->library("email");

        //configuracion para gmail
		$configGmail = array(
			'protocol' => 'smtp',
			'smtp_host' => 'smtp.gmail.com',
			'smtp_crypto' => 'ssl',
			'smtp_port' => 465,
			'smtp_user' => getenv('EMAIL_CONF_USER'),
			'smtp_pass' => getenv('EMAIL_CONF_PASS'),
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'newline' => "\r\n"
			);

        //cargamos la configuración para enviar con gmail
		$this->email->initialize($configGmail);

		$this->email->from($de);
		$this->email->to($para);

		$this->email->subject($asunto);
		$this->email->message($mensaje);
		return $this->email->send();
		//var_dump($this->email->print_debugger()); 
	}
}
