<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    Documentacion API
    <hr>
    Path: v1/constructoras
    <p>Este EP podras crear y consultar sobre constructoras</p>
    <p>Para obtener todas las constructoras se debe hacer una peticion <strong>GET</strong> de la siguiente manera desde la app</p>
    <code> request.get('v1/constructoras?id=0') </code> <span>id debe ser 0</span>
    <p>si id es un valor distinto a 0, este EP regresara una lista de desarrollos que pertenecen a esa constructora</p>
    <hr>
</body>
</html>