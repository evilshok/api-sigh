<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Empresa extends Base{
    function __construct(){
        parent::__construct();
        $this->load->model('admin/empresa');
    }

    function index_post(){
        
        $new = [
            'nombre' => $this->request('nombre'),
            'coleccion' => $this->request('coleccion'),
        ];

        $insert = $this->empresa->create($new);

        if($insert == null){
            return $this->response([
                'message' => 'Error al crear la empresa'
            ],500);
        }
        
        return $this->response([
            'message' => 'Empresa creada con exito'
        ],200);
        
    }
}