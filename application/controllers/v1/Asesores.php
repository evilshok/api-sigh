<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asesores extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_asesor');
    }

    function index_get(){
        $asesores = $this->m_asesor->all();

        return $this->response(compact('asesores'));
    }


}?>
