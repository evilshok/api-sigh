<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Analisis extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_expediente');
        $this->load->model('v1/m_fases');
    }

    function index_post(){
		if(! $this->validar_acceso_usuario(17)){
			//a este enpoint solo pueden acceder usuarios que tengan el permiso 16 (Mesa de control) + 1 (administrador)
			$this->response([
                'message' => 'Usted no tiene los permisos necesarios para acceder a esta seccion o efecturar algun movimiento, solicitelos'
            ], 403);
		}
		$id = $this->request('id');
		$fecha_vencimiento = $this->request('fecha_vencimiento');
		$fecha_analisis = $this->request('fecha_analisis');
		$tasa = $this->request('tasa');
		$monto_aprobado = $this->request('monto_aprobado');

		$exists = $this->m_expediente->exists($id);

		if( !$exists ){
			$this->response([
                'message' => 'El caso que esta intentando actualizar no existe'
            ], 400);
		}
		$exists_folio_soc = $this->m_expediente->exists_folio_soc($id);

		if($exists_folio_soc['folio_soc'] === null){
			$this->response([
				'message' => 'El caso no puede ser avanzado de etapa, es necesario capturar el FOLIO SOC',
				"extras" =>'REQUIRE_UPDATE_FOLIO_SOC'
            ], 400);
		}

		$response = $this->m_analisis->avanzar_analisis($id,$fecha_vencimiento,$fecha_analisis,$tasa,$monto_aprobado);

		if( $response == null){
			$this->response([
                'message' => 'No fue posible avanzar el caso a Avaluo'
            ], 400);
		} 
		// TODO: enviar correo a asesor y cliente sobre el avance
		return $this->response([
            'message' => 'Expendiente avanzado'
        ], 201);


    }

    function index_get(){

		$id = $this->get('id');

		$conditions = [
			'fase' => 'Analisis'
		];
		
		$response = (isset($id)) ? $this->m_expediente->get($id) : $this->m_expediente->getAll($conditions) ;
        if(! $response ) {
            $this->response([
                'message' => 'El caso que esta buscando no existe'
            ], 400);
        }
        $caso = array_map([$this,'map_caso'],$response);
    
        return $this->response(compact('caso'));

    }
    
}
