<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Inmobiliaria extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_inmobiliaria');
    }

    function index_post(){
        $nombre = $this->request('nombre');

        $existe = $this->m_inmobiliaria->existe($nombre);

        if($existe) {
            $this->response([
                'message' => 'La Inmobiliaria ya existe'
            ], 400);
        }

        $id_inmobiliaria = $this->m_inmobiliaria->create($nombre);

        if($id_inmobiliaria == null) {
            return $this->response([
                'message' => 'Error al momento de registrar la inmobiliaria'
            ], 500);
        }

        return $this->response([
            'message' => 'Inmobiliaria registrada'
        ], 201);
        
    }

    function index_get(){

    }

    function index_put(){

    }

    function index_delete(){
        
    }

}?>
