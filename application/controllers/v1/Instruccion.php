<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Instruccion extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_expediente');
        $this->load->model('v1/m_fases');
    }

    function index_post(){
		if(! $this->validar_acceso_usuario(17)){
			//a este enpoint solo pueden acceder usuarios que tengan el permiso 16 (Mesa de control) + 1 (administrador)
			$this->response([
                'message' => 'Usted no tiene los permisos necesarios para acceder a esta seccion o efecturar algun movimiento, solicitelos'
            ], 403);
		}
        $id = $this->request('id');
        $fecha_instruccion = $this->request('fecha_instruccion');
        $monto_firmado = $this->request('monto_firmado');
		$tasa = $this->request('tasa');
		
        $id_asesor = $this->getUid();

        if($id_asesor == null) {
            $this->response([
                'message' => 'No tiene permisos para hacer este movimiento'
            ], 400);
        }

        $avaluo = $this->m_fases->avanzar_instruccion($id,$fecha_instruccion,$monto_firmado,$tasa);

        if($avaluo == null) {
            $this->response([
                'message' => 'Ocurrio un error al avanzar el caso'
            ], 400);
		}
		
        return $this->response([
            'message' => 'Expendiente avanzado'
        ], 201);
    }

    function index_get(){
		$id = $this->get('id');

		$conditions = [
			'fase' => 'Instruccion'
		];
		
		$response = (isset($id)) ? $this->m_expediente->get($id) : $this->m_expediente->getAll($conditions) ;
        if(! $response ) {
            $this->response([
                'message' => 'El no existen casos en esta etapa'
            ], 400);
        }
        $caso = array_map([$this,'map_caso'],$response);
    
        return $this->response(compact('caso'));
    }

}
