<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Desarrollo extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_desarrollo');
    }

    function index_post(){
        $nombre = $this->request('nombre');
        $id_constructora = $this->request('id_constructora');

        $existe = $this->m_desarrollo->existe($nombre);

        if($existe) {
            $this->response([
                'message' => 'El Desarrollo ya existe'
            ], 400);
        }

        $id_desarrollo = $this->m_desarrollo->create($nombre,$id_constructora);

        if($id_desarrollo == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el desarrollo'
            ], 500);
        }

        return $this->response([
            'message' => 'Desarrolo registrado'
        ], 201);
        
    }

    function index_get(){

    }

    function index_put(){

    }

    function index_delete(){
        
    }

}?>
