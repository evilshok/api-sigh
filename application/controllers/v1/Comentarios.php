<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Comentarios extends Base{

    function __construct(){
        parent::__construct();
		$this->load->model('v1/m_comentarios');
		$this->load->model('v1/m_fases');
    }
	function index_post(){
		$id_caso = $this->request('id_caso');		
		$comentario = $this->request('comentario');
		$usuario = "Tadeo"; //nombre del usuario; se debe obtener de la sesion del usuario
		$fase = $this->request('fase'); //para conocer en que fase o etapa se genero el comentario
		// $id_usuario = $this->getUid();
		$id_usuario = 1;

		$id_fase = $this->m_fases->get_id_fase($fase);

		$response = $this->m_comentarios->create($id_caso,$comentario,$id_fase,$id_usuario,$usuario);

		if( $response == null){
			$this->response([
                'message' => 'Ocurrio un error al ingresar el comentario'
            ], 400);
		}

		$this->response([
			'message' => 'Comentario registrado'
		], 200);
		

	}
	function index_get($id_caso){
		$comentarios = $this->m_comentarios->obtener_comentarios($id_caso);
		return $this->response(compact('comentarios'));
	}
}
