<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fondeo extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_expediente');
        $this->load->model('v1/m_fases');
    }

    function index_post(){
		if(! $this->validar_acceso_usuario(17)){
			//a este enpoint solo pueden acceder usuarios que tengan el permiso 16 (Mesa de control) + 1 (administrador)
			$this->response([
                'message' => 'Usted no tiene los permisos necesarios para acceder a esta sección o efecturar algún movimiento, solicitelos'
            ], 403);
		}
        $id = $this->request('id');
        $fecha_fondeo = $this->request('fecha_fondeo');
        $escrituras = $this->request('escrituras');
		$monto_firmado = $this->request('monto_firmado');
		$notaria = $this->request('notaria');
		$valuador = $this->request('valuador');
		
        $id_asesor = $this->getUid();

        if($id_asesor == null) {
            $this->response([
                'message' => 'No tiene permisos para hacer este movimiento'
            ], 400);
        }

        $avaluo = $this->m_fases->avanzar_fondeo($id,$fecha_fondeo,$escrituras,$monto_firmado,$notaria,$valuador);

        if($avaluo == null) {
            $this->response([
                'message' => 'Ocurrio un error al avanzar el caso'
            ], 400);
		}
		
        return $this->response([
            'message' => 'Expendiente avanzado'
        ], 201);
    }

    function index_get(){
		$id = $this->get('id');

		$conditions = [
			'fase' => 'Fondeo'
		];
		
		$response = (isset($id)) ? $this->m_expediente->get($id) : $this->m_expediente->getAll($conditions) ;
        if(! $response ) {
            $this->response([
                'message' => 'El no existen casos en esta etapa'
            ], 400);
        }
        $caso = array_map([$this,'map_caso'],$response);
    
        return $this->response(compact('caso'));
    }

}
