<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Constructora extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_constructora');
        $this->load->model('v1/m_desarrollo');
    }

    function index_post(){
        $nombre = $this->request('nombre');

        $existe = $this->m_constructora->existe($nombre);

        if($existe) {
            $this->response([
                'message' => 'La Constructora ya existe'
            ], 400);
        }

        $id_constructora = $this->m_constructora->create($nombre);

        if($id_constructora == null) {
            return $this->response([
                'message' => 'Error al momento de registrar la constructora'
            ], 500);
        }

        return $this->response([
            'message' => 'Constructora registrada'
        ], 201);

    }

    function index_get(){
        $id = intval($this->get('id'));
        $constructora = $this->m_desarrollo->getByContructora($id);
        return $this->response(compact('desarrollos'));
    }

    function index_put(){

    }

    function index_delete(){
        
    }

    function all_get(){
        $constructoras = $this->m_constructora->getAll();
        return $this->response(compact('constructoras'),200);
    }

}?>
