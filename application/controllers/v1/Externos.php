<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Externos extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_externos');
    }

    function index_post(){
        $nombre = $this->request('nombre');
        $correo = $this->request('correo');
        $id_banco = $this->request('id_banco');
        $id_constructora = $this->request('id_constructora');
        $id_desarrollo = $this->request('id_desarrollo');
        $id_inmobiliaria = $this->request('id_inmobiliaria');

        $existe = $this->m_externos->existe($correo);
        if($existe) {
            $this->response([
                'message' => 'El Asesor ya existe'
            ], 400);
        }

        $id_externo = $this->m_externos->create($nombre, $correo, $id_banco, $id_constructora, $id_desarrollo, $id_inmobiliaria);

        if($id_externo == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el asesor externo'
            ], 500);
        }

        $id_asesor_externo = $this->m_externos->insert_externo($id_externo['id']);
        
        if($id_asesor_externo == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el asesor externo'
            ], 500);
        }

        return $this->response([
            'message' => 'Asesor registrado'
        ], 201);

    }

    function index_get(){

    }

}
