<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pago extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_expediente');
        $this->load->model('v1/m_fases');
    }

    function index_post(){
		if(! $this->validar_acceso_usuario(1)){
			//a este enpoint solo pueden acceder usuarios que tengan el permiso 1 (administrador)
			$this->response([
                'message' => 'Usted no tiene los permisos necesarios para acceder a esta sección o efecturar algún movimiento, solicitelos'
            ], 403);
		}
    }

    function index_get(){
		$id = $this->get('id');
		//TODO: cambiar fase por id
		$conditions = [
			'fase' => 'Pago'
		];
		
		$response = (isset($id)) ? $this->m_expediente->get($id) : $this->m_expediente->getAll($conditions) ;
        if(! $response ) {
            $this->response([
                'message' => 'El no existen casos en esta etapa'
            ], 400);
        }
        $caso = array_map([$this,'map_caso'],$response);
    
        return $this->response(compact('caso'));
    }

}
