<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Perfil extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_expedientes_fases');
        $this->load->model('v1/m_asesor');
    }

    function expedientes_fases_get(){
        $id = $this->getUid();
        $asesor = $this->m_asesor->get($id);

        switch ($asesor['tipo_usuario']) {
            case '1':
                $_array = $this->admin();
                break;
            
            default:
                # code...
                break;
        }

        return $this->response($_array,200);
    }

    protected function admin(){
        $condicion = [];
        $analisis = $this->m_expedientes_fases->cantidad('Analisis',$condicion);
        $autorizados = $this->m_expedientes_fases->cantidad('Autorizados',$condicion);
        $avaluo = $this->m_expedientes_fases->cantidad('Avaluo',$condicion);
        $instruccion = $this->m_expedientes_fases->cantidad('Instruccion',$condicion);
        $firma = $this->m_expedientes_fases->cantidad('Firma',$condicion);
        $fondeo = $this->m_expedientes_fases->cantidad('Fondeo',$condicion);
        $cobranza = $this->m_expedientes_fases->cantidad('Cobranza',$condicion);
        $comision = $this->m_expedientes_fases->cantidad('Comision',$condicion);
        $pago = $this->m_expedientes_fases->cantidad('Pago',$condicion);

        // las siguientes etapas no son fases, se consulta diferente
        // $vencidos = $this->m_expedientes_fases->cantidad('vencidos',$condicion);
        // $por_vencer = $this->m_expedientes_fases->cantidad('por_vencer',$condicion);
        // $por_integrar = $this->m_expedientes_fases->cantidad('por_integrar',$condicion);
        // $integrados  = $this->m_expedientes_fases->cantidad('integrados',$condicion);
        return compact('analisis','autorizados','avaluo','instruccion','firma', 'fondeo', 'cobranza', 'comision', 'pago');
    }
}
