<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Fases extends Base{

    function __construct(){
        parent::__construct();
		$this->load->model('v1/m_fases');
    }
	function casos_get($fase){
		
		$condiciones = [
			'id_fase' => $this->get_id_fase($fase)
		];

		$casos = $this->m_fases->obtener_casos_por_fase($condiciones);

		return $this->response(compact('casos'));
	}
	function index_get($id_caso){
	
	}
	protected function get_id_fase($fase){
		return $this->m_fases->get_id_fase($fase);
	}
	function analisis_post($id_caso){
		$fecha_autorizacion = $this->request('fecha_autorizacion');
		$fecha_vencimiento 	= $this->request('fecha_vencimiento');
		$monto_autorizado 	= $this->request('monto_autorizado');
		$tasa 				= $this->request('tasa');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'monto_autorizado'=> $monto_autorizado,
			'tasa'=> $tasa,
			'id_fase' => 2
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'autorizado'=> $fecha_autorizacion,
			'vencimiento'=> $fecha_vencimiento,
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);
	}
	function autorizado_post($id_caso){
		$fecha_avaluo = $this->request('fecha_avaluo');
		$id_notaria = $this->request('id_notaria');
		$id_valuador = $this->request('id_valuador');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'id_notaria'=> $id_notaria,
			'id_valuador'=> $id_valuador,
			'id_fase' => 3
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'avaluo'=> $fecha_avaluo,
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);

	}
	function avaluo_post($id_caso){
		$fecha_instruccion = $this->request('fecha_instruccion');
		$fecha_riesgos = $this->request('fecha_riesgos');
		$id_notaria = $this->request('id_notaria');
		$id_valuador = $this->request('id_valuador');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'id_notaria'=> $id_notaria,
			'id_valuador'=> $id_valuador,
			'fecha_riesgos'=> $fecha_riesgos,
			'id_fase' => 4
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'instruccion'=> $fecha_instruccion,
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);

	}
	function instruccion_post($id_caso){
		$fecha_firma = $this->request('fecha_firma');
		$monto_firmado = $this->request('monto_firmado');
		$tasa = $this->request('tasa');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'monto_firmado'=> $monto_firmado,
			'tasa'=> $tasa,
			'id_fase' => 5
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'firma'=> $fecha_firma,
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);

	}
	function firma_post($id_caso){
		$fecha_fondeo = $this->request('fecha_fondeo');
		$monto_firmado = $this->request('monto_firmado');
		$escrituras = $this->request('escrituras');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'monto_firmado'=> $monto_firmado,
			'escrituras'=> $escrituras,
			'id_fase' => 6
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'fondeo'=> $fecha_fondeo,
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);

	}
	function fondeo_post($id_caso){
		$fecha_cobranza = $this->request('fecha_cobranza');
		$monto_firmado = $this->request('monto_firmado');
		$escrituras = $this->request('escrituras');
		$id_notaria = $this->request('id_notaria');
		$id_valuador = $this->request('id_valuador');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'monto_firmado'=> $monto_firmado,
			'escrituras'=> $escrituras,
			'id_notaria' => $id_notaria,
			'id_valuador' => $id_valuador,
			'id_fase' => 7
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'cobranza'=> $fecha_cobranza,
			'firmado' => 1
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);

	}
	function cobranza_post($id_caso){
		$fecha_comision = $this->request('fecha_comision');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'id_fase' => 8
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'comision'=> $fecha_comision,
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);

	}
	function comision_post($id_caso){
		$fecha_pago = $this->request('fecha_pago');

		$caso_actualizado = $this->m_fases->actualizar_caso($id_caso,[
			'id_fase' => 9
		]);

		if(!$caso_actualizado){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
		
		$fase_actualizada = $this->m_fases->actualizar_fases($id_caso,[
			'pago'=> $fecha_pago,
			]);

		if(!$fase_actualizada){
			return $this->response(['message' => 'Error al intentar actualizar el caso'],500);
		}
			
		return $this->response(['message' => 'Caso actualizado'],200);

	}
	function cantidad_etapas_get(){
		//TODO: validar el tipo de usuario para consultar cantidades
		$analisis = $this->m_fases->cantidad_etapas(1);
		$autorizados = $this->m_fases->cantidad_etapas(2);
		$avaluo = $this->m_fases->cantidad_etapas(3);
		$instruccion = $this->m_fases->cantidad_etapas(4);
		$firma = $this->m_fases->cantidad_etapas(5);
		$fondeo = $this->m_fases->cantidad_etapas(6);
		$cobranza = $this->m_fases->cantidad_etapas(7);
		$comision = $this->m_fases->cantidad_etapas(8);
		$vencidos = $this->m_fases->vencidos();
		$por_vencer = $this->m_fases->por_vencer();
		return $this->response([
			"analisis" => $analisis,
			"autorizados" => $autorizados,
			"avaluo" => $avaluo,
			"instruccion" => $instruccion,
			"firma" => $firma,
			"fondeo" => $fondeo,
			"cobranza" => $cobranza,
			"comision" => $comision,
		],200);
	}
}
