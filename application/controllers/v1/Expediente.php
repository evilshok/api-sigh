<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Expediente extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_expediente');
        $this->load->model('v1/m_constructora');
        $this->load->model('v1/m_desarrollo');
        $this->load->model('v1/m_inmobiliaria');
        $this->load->model('v1/m_bitacora');
    }

    function nuevo_post($id){
        $id_apoyo = $this->request('id_apoyo');
        $id_esquema = $this->request('id_esquema');
        $id_banco = $this->request('id_banco');
        $id_cliente_asesor = $id;
		$fecha_ingreso_banco = $this->request('fecha_ingreso_banco');
        $monto_solicitado = $this->request('monto_solicitado');
        $ejecutivo_banco = $this->request('ejecutivo_banco');
        $plazo = $this->request('plazo');
        $tasa = $this->request('tasa');
        $factor_pago = $this->request('factor_pago');
        $comision_apertura = $this->request('comision_apertura');
        $pago_adelantado = $this->request('pago_adelantado');
        $incremento_factor_pago = $this->request('incremento_factor_pago');
        $folio_soc = $this->request('folio_soc');
        $folio_banco = $this->request('folio_banco');

        // $id_usuario = $this->getUid();
        $id_usuario = 1;

        if($id_cliente_asesor == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el caso, debe seleccionar un cliente'
            ], 500);
        }

        if($id_usuario == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el caso'
            ], 500);
        }

        if($fecha_ingreso_banco == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el caso, debe ingresar la fecha de alta'
            ], 500);
        }

        if($monto_solicitado == null || $monto_solicitado == 0) {
            return $this->response([
                'message' => 'Error al momento de registrar el caso, debe ingresar un monto mayor a 0'
            ], 500);
        }

        if($tasa == null || $tasa == 0) {
            return $this->response([
                'message' => 'Error al momento de registrar el caso, debe ingresar la tasa'
            ], 500);
        }

        $id_expediente = $this->m_expediente->create($id_usuario,$id_cliente_asesor,$id_apoyo,$id_banco,$id_esquema,$fecha_ingreso_banco,$tasa,$monto_solicitado,$ejecutivo_banco,$plazo,$factor_pago,$comision_apertura,$pago_adelantado,$folio_banco,$folio_soc,$incremento_factor_pago);

        if($id_expediente == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el caso'
            ], 500);
        }
		// $insert_bitacora = $this->m_bitacora->insert_bitacora($id_expediente,'Analisis',date('Y/m/d'));
		
        return $this->response([
            'message' => 'Caso registrado'
        ], 201);

    }

    function index_get($id){
        $caso = $this->m_expediente->obtener_datos_planos_expediente($id);
        return $this->response(compact('caso'));
    }

    function detalles_get($id){
        $caso = $this->m_expediente->get($id);

        if($caso == null) {
            return $this->response([
                'message' => 'Error al momento de obtener el caso seleccionado'
            ], 500);
        }

        if($caso['id_constructora'] > 0){
            $constructora = $this->m_constructora->get($caso['id_constructora']);
            $caso['constructora'] = $constructora['nombre_constructora'];
        }

        if($caso['id_desarrollo'] > 0){
            $desarrollo = $this->m_desarrollo->get($caso['id_desarrollo']);
            $caso['desarrollo'] = $desarrollo['nombre_desarrollo'];
        }

        if($caso['id_mercado'] > 0){
            $inmobiliaria = $this->m_inmobiliaria->get($caso['id_inmobiliaria']);
            $caso['mercado'] = $constructora['nombre'];
        }

        $caso = $this->mapCaso($caso);

        return $this->response(compact('caso'));

    }

    function cancelar_put($id){
        $razon = $this->request('razon');
        $cancelado = $this->m_expediente->cancelar_caso($id,$razon);
        return $this->response(['message' => "Caso cancelado"],200);
    }

    function mapCaso($object){
        $array = [
            "id_caso" => $object['id_caso'],
            "id_cliente_asesor" => $object['id_cliente_asesor'],
            "cliente" => $object['cliente'],
            "id_banco" => $object['id_banco'],
            "nombre_banco" => $object['banco_nombre'],
            "id_esquema" => $object['nombre_esquema'],
            "nombre_esquema" => $object['nombre_esquema'],
            "fecha_ingreso_banco" => $object['ingreso_banco'],
            "fase" => $object['fase'],
            "notaria" => $object['notaria'],
            "valuador" => $object['valuador'],
            "monto_solicitado" => $object['monto_solicitado'],
            "monto_aprobado" => $object['monto_aprobado'],
            "monto_firmado" => $object['monto_firmado']
        ];

        if($object['id_constructora'] > 0){
            $array["id_constructora"] = $object['id_constructora'];
            $array["constructora"] = $object['constructora'];
        }

        if($object['id_desarrollo'] > 0){
            $array["id_desarrollo"] = $object['id_desarrollo'];
            $array["desarrollo"] = $object['desarrollo'];
        }

        if($object['id_mercado'] > 0){
            $array["id_inmobiliaria"] = $object['id_mercado'];
            $array["inmobiliaria"] = $object['mercado'];
        }

        return $array;
    }

    function expedientes_get($id_cliente_asesor){
        if( !$this->m_expediente->existen_expedientes_cliente($id_cliente_asesor)){
            return $this->response([ 'message '=> 'No existen registos para este cliente'], 404);
        }
        $activos = $this->m_expediente->expedientes_activos($id_cliente_asesor);
        $no_activos = $this->m_expediente->expedientes_no_activos($id_cliente_asesor);

        return $this->response(compact('activos','no_activos'),200);
    }

    function asesores_expediente_get($id){
        $this->load->model('v1/m_asesor');
        $asesor = $this->m_expediente->asesor_expediente($id);
        $mesa_control = $this->m_asesor->asesores_mesa_control();

        return $this->response(compact('asesor','mesa_control'),200);
    }
}?>
