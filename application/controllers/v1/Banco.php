<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Banco extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_banco');
        $this->load->model('v1/m_esquema');
    }

    function index_post(){
        $nombre = $this->request('nombre');

        $existe = $this->m_banco->existe($nombre);

        if($existe) {
            $this->response([
                'message' => 'El Banco ya existe'
            ], 400);
        }

        $id_banco = $this->m_banco->create($nombre);

        if($id_banco == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el banco'
            ], 500);
        }

        return $this->response([
            'message' => 'Banco registrado'
        ], 201);

    }

    function index_get(){
        return $this->response(['message' => 'banco'],200);
    }

    function index_put(){

    }

    function index_delete(){
        
    }

    function all_get(){
        $bancos = $this->m_banco->getAll();
        return $this->response(compact('bancos'),200);
    }

    function esquemas_get($id){
        $esquemas = $this->m_esquema->lista_esquemas($id);
        return  $this->response(compact('esquemas'));
    }

}?>
