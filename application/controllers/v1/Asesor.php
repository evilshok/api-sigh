<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Asesor extends Base{
    protected $coleccion = "";
    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_asesor');
        //obtener la coleccion de la sesion del usuario que esta enviando la peticion
        $this->coleccion = "sigh_db";
        $this->loadDatabase($this->coleccion);
    }

    function nuevo_post(){

        $nombre = $this->request('nombre');
        $correo = $this->request('correo');
        $tipo_usuario = $this->request('tipo_usuario');
        $id_oficina = $this->request('id_oficina');
        $id_banco = $this->request('id_banco');
        $id_constructora = $this->request('id_constructora');
        $id_desarrollo = $this->request('id_desarrollo');
        $id_inmobiliaria = $this->request('id_inmobiliaria');
        $permisos = $this->request('permisos');

        $exists = $this->m_asesor->exists($correo);

        if( $exists ){
            return $this->response([ 'message' => 'El usuario ya existe' ], 200);
        }
        $password = substr(md5(rand()),0,8);

        $objeto_insertar = [
            'nombre' => $nombre,
            'correo' => $correo,
            'tipo_usuario' => $tipo_usuario,
            'id_oficina' => $id_oficina,
            'id_banco' => $id_banco,
            'id_constructora' => $id_constructora,
            'id_desarrollo' => $id_desarrollo,
            'id_inmobiliaria' => $id_inmobiliaria,
            'permisos' => json_encode($permisos),
            'password' => md5($password),
            'coleccion' => $this->coleccion
        ];
        $created = $this->m_asesor->create($objeto_insertar);
        // die(json_encode($created));
        if( $created == null){
            return $this->response(['error' => $created], 400);
        }
        // TODO: enviar correo de bienvenida al usuario
        return $this->response(['message' => 'success'], 200);
    }

    function index_get($id){
        $existe = $this->m_asesor->existe_id($id);

        if(! $existe) {
            $this->response([
                'message' => 'El asesor que esta buscando no existe'
            ], 400);
        }
        $asesor = $this->m_asesor->get($id);

        if($asesor == null){
            $this->response([
                'message' => 'El asesor que esta buscando no existe'
            ], 400);
        }

        return $this->response(compact('asesor'));
    }

    function index_put(){

    }

    function index_delete(){
        
    }

}?>
