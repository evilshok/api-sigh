<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Valuador extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_valuador');
    }

    function index_post(){
        $nombre = $this->request('nombre');
        $correo = $this->request('correo');
        $telefono = $this->request('telefono');

        $existe = $this->m_valuador->existe($nombre);

        if($existe) {
            $this->response([
                'message' => 'El Valuador ya existe'
            ], 400);
        }

        $id_valuador = $this->m_valuador->create($nombre,$correo,$telefono);

        if($id_valuador == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el valuador'
            ], 500);
        }

        return $this->response([
            'message' => 'Valuador registrado'
        ], 201);
    }

    function index_get(){
        $valuadores = $this->m_valuador->get_all();
        return $this->response(compact('valuadores'));
    }

    function index_put(){

    }

    function index_delete(){
        
    }

}?>
