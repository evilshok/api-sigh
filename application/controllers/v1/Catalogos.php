<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Catalogos extends Base{

	function __construct(){
        parent::__construct();
		$this->load->model('v1/m_banco');
		$this->load->model('v1/m_constructora');
	}
	
	function apoyos_get(){
		$apoyos = $this->m_banco->getApoyos();
		return $this->response(compact('apoyos'));
	}
}
