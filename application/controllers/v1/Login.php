<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends Recursos {

    function index_post() {
        $this->load->model('v1/m_asesor');
        $this->load->library('jwt');

        $this->validate([
            ['field' => 'correo', 'label' => 'Correo', 'rules' => 'required'],
            ['field' => 'clave', 'label' => 'Clave', 'rules' => 'required'],
        ]);

        $credenciales = $this->request(['correo', 'clave']);

        $asesor = $this->m_asesor->get_con_correo_clave(
            $credenciales['correo'], 
            md5($credenciales['clave'])
        );

        if( is_null($asesor)) {
            return $this->response([
                'error' => 'bad/login',
                'message' => 'El correo o la contraseña son incorrectos'
            ], 401);
        }

        $data_usuario = [
            'id' => $asesor['id'],
            'nombre' => $asesor['nombre'],
            'correo' => $asesor['correo'],
            'tipo_usuario' => $asesor['tipo_usuario'],
            'id_oficina' => $asesor['id_oficina'],
            'id_rangos' => $asesor['id_rangos'],
            'permisos' => $asesor['permisos'],
            'coleccion' => $asesor['coleccion'],
            'id_banco' => $asesor['id_banco'],
            'id_constructora' => $asesor['id_constructora'],
            'id_desarrollo' => $asesor['id_desarrollo'],
            'id_inmobiliaria' => $asesor['id_inmobiliaria'],
        ];

        
        $token = $this->jwt->generateServerToken($asesor['id_asesor'], $data_usuario);
        
        $exp = $this->jwt->getExpiration();

        $exp_mili = $exp * 1000;

        return $this->response(compact('token', 'data_usuario', 'exp', 'exp_mili'));

    }

}

