<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Esquemas extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_esquema');
	}
	
    function index_get($id_banco){

		$esquemas = $this->m_esquema->lista_esquemas($id_banco);
		
		return $this->response(compact('esquemas'));

    }

    function index_put(){

    }

    function index_delete(){
        
    }

}?>
