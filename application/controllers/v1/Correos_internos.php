<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Correos_internos extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_expediente');
    }

    function enviar_post(){
        $id_expedinte = $this->request('id_expediente');
        $expediente = $this->m_expediente->get($id_expedinte);

        $correos = $this->request('correos');

        $mensaje = "El mensaje de se encuentra despues de estas lineas <br/> ------------- <br/>".$this->request('mensaje');

        $asunto = 'Correo desde SIGH - '.$expediente['cliente'].' - Fase: '.$expediente['fase'] ;

        //TODO: se debe agregar el correo de la sesion/token
        $enviado = $this->enviarCorreo('tzamora@mactenova.mx',$correos,$asunto,$mensaje);

        return $this->response($asunto,200);

    }
}?>
