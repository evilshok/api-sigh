<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Esquema extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_esquema');
    }

    function index_post(){
        $esquema = $this->request('esquema');
        $id_banco = $this->request('id_banco');

        $existe = $this->m_esquema->existe($esquema,$id_banco);

        if($existe) {
            $this->response([
                'message' => 'El Esquema ya existe'
            ], 400);
        }

        $id_esquema = $this->m_desarrollo->create($esquema,$id_banco);

        if($id_esquema == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el esquema'
            ], 500);
        }

        return $this->response([
            'message' => 'Esquema registrado'
        ], 201);
    }

    function index_get(){

    }

    function index_put(){

    }

    function index_delete(){
        
    }

}?>
