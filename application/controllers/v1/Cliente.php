<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cliente extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_cliente');
    }

    function index_post(){
        $curp = $this->request('curp');
        $ap_paterno = $this->request('ap_paterno');
        $ap_materno = $this->request('ap_materno');
        $nombres = $this->request('nombres');
        $fecha_nacimiento = $this->request('fecha_nacimiento');
        $correo = $this->request('correo');
        $telefono = $this->request('telefono');
        $ingresos = $this->request('ingresos');
        $valor_vivienda = $this->request('valor_vivienda');
        $tipo_economina = $this->request('tipo_economina');
        $tipo_compra = $this->request('tipo_compra');
        $referido = $this->request('referido');
        $id_constructora = $this->request('id_constructora');
        $id_desarrollo = $this->request('id_desarrollo');
        $id_inmobiliaria = $this->request('id_inmobiliaria');
        
        // $id_asesor = $this->getUid();
        //TODO: quitar cuando se agregue login
        $id_asesor = 1;

        if($curp == null){
            $this->response([
                'message' => 'Es necesario proporcionar la curp'
            ], 400);
        }

		$existe = $this->m_cliente->existe(strtoupper($curp));
        //TODO: se debe verificar si el cliente ya se firmo para poder hacer un nuevo registro
        if($existe) {
            $this->response([
                'message' => 'El Cliente ya existe'
            ], 400);
        }

        $id_cliente_asesor = $this->m_cliente->create($id_asesor,$curp,$ap_paterno,$ap_materno,$nombres,$fecha_nacimiento,$correo,$telefono,$ingresos,$tipo_economina);

        if($id_cliente_asesor == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el cliente'
            ], 500);
        }

        $id_esquema_compra = $this->m_cliente->add_esquema_compra($id_cliente_asesor,$id_constructora,$id_desarrollo,$id_inmobiliaria,$valor_vivienda);

        if($id_esquema_compra == null) {
            return $this->response([
                'message' => 'Error al momento de registrar el esquema de compra'
            ], 500);
        }

        return $this->response([
            'message' => 'Cliente registrado'
        ], 201);

    }

    function index_get(){
        $curp = trim($this->get('curp'));

        $existe = $this->m_cliente->existe(strtoupper(trim($curp)));

        if(! $existe) {
            $this->response([
                'message' => 'El Cliente que esta buscando no existe'
            ], 401);
        }
        $cliente = $this->m_cliente->get($id);

        if($cliente == null){
            $this->response([
                'message' => 'El Cliente que esta buscando no existe'
            ], 400);
        }

        return $this->response(compact('cliente'));
    }

    function index_put(){

    }

    function index_delete(){
        
    }

    function lista_get(){
        $lista = $this->m_cliente->get_all();
        return $this->response(compact('lista'));
    }

    function detalles_get($id){
        $detalles = $this->m_cliente->get($id);
        return $this->response(compact('detalles'));
    }

}?>
