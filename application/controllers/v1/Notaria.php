<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notaria extends Base{

    function __construct(){
        parent::__construct();
        $this->load->model('v1/m_notaria');
    }

    function index_post(){
        $numero = $this->request('numero');
        $nombre = $this->request('nombre');
        $ciudad = $this->request('ciudad');

        $existe = $this->m_notaria->existe($nombre);

        if($existe) {
            $this->response([
                'message' => 'La Notaria ya existe'
            ], 400);
        }

        $id_notaria = $this->m_notaria->create($numero,$nombre,$ciudad);

        if($id_notaria == null) {
            return $this->response([
                'message' => 'Error al momento de registrar la notaria'
            ], 500);
        }

        return $this->response([
            'message' => 'Notaria registrada'
        ], 201);
    }

    function index_get(){
        $notarias = $this->m_notaria->get_all();
        return $this->response(compact('notarias'));
    }

    function index_put(){

    }

    function index_delete(){
        
    }

}?>
