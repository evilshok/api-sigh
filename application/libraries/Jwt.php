<?php defined('BASEPATH') or exit('No direct script access allowed');

use Lcobucci\JWT\Parser;
use Lcobucci\JWT\ValidationData;
use Lcobucci\JWT\Builder;
use Lcobucci\JWT\Signer\Key;
use Lcobucci\JWT\Signer\Rsa\Sha256;

use function GuzzleHttp\json_encode;

class Jwt
{
    protected $token;

    protected $expiration = 7200; # 2 hrs
    
    public function verifyAndGetUid($token)
    {
        $token = (new Parser())->parse((string) $token);
        
        return $this->serverAuth($token);
        
    }

    /** Method to generate a server token
     *  @param string $uid Unique identificator for the user.
     *  @param array $data Data for the token.
     *  @return string 
     */
    public function generateServerToken($uid, $data = null) {
        
        $server_token = $this->createServerToken($uid, $data);

        return $server_token;
    }

    /** This token is only for Development envionroment, no production
     *  @param $uid User unique identificator.
     *  @return string The Token only for dev.
     */
    public function generateDevToken(string $uid) {
        $server_dev_token = $this->createServerToken(
            $uid,
            ['only_dev' => TRUE]
        );

        return $server_dev_token;
    }

    protected function serverAuth($token)
    {

        $signer = new Sha256();

        $publicKey = new Key('file://' . __DIR__ . '/../jwtRS256.key.pub');
        
        $data = new ValidationData(); // It will use the current time to validate (iat, nbf and exp)

        $validate_token = $token->validate($data);

        if( ! $validate_token ) {

            $exp = $token->getClaim('exp');

            http_response_code(401);

            $date_expire = date('Y-m-d H:i:s', $exp); 

            $resp = [
                'error' => 'Invalidate JWT',
                'status' => 401,
                'code' => 'token/expired',
                'errors' => "El token ya expiró el {$date_expire}",
            ];

            die(json_encode($resp));
        }

        $token_pass = $token->verify($signer, $publicKey);

        if ( ! $token_pass ) {
            http_response_code(401);

            $resp = [
                'error' => 'Invalidate JWT',
                'status' => 401,
                'code' => 'token/invalid',
                'errors' => "El token no pertenece a este servidor",
            ];

            die(json_encode($resp));
        }

        $token_data = $token->getClaim('data');

        if ( ENVIRONMENT == 'production' && isset($token_data) ) {
            
            if ( property_exists($token_data, 'only_dev') ) {

                http_response_code(401);

                $resp = [
                    'error' => 'Invalidate JWT',
                    'status' => 401,
                    'code' => 'token/invalid',
                    'errors' => "El token es solo para el entorno de desarrollo.",
                ];

                die(json_encode($resp));
            }
        }


        return $token->getClaim('uid');
    }


    /** Method to generate custom token for validation by the server.
     *  @param $uid User unique identificator.
     *  @param $additional_claims Additional data for the token
     *  @return string The Token for the server.
     */
    protected function createServerToken($uid, $additional_claims = NULL) {
        $signer = new Sha256();
        $privateKey = new Key('file://' . __DIR__ . '/../jwtRS256.key');
        $time = time();

        $token = (new Builder())->setIssuer(base_url()) // Configures the issuer (iss claim)
            ->setAudience(base_url()) // Configures the audience (aud claim)
            ->setId(uniqid(), true) // Configures the id (jti claim), replicating as a header item
            ->setIssuedAt($time) // Configures the time that the token was issued (iat claim)
            ->setNotBefore($time) // Configures the time that the token can be used (nbf claim)
            ->setExpiration($time + $this->expiration) // Configures the expiration time of the token (exp claim)
            ->set('uid', $uid) // Configures a new claim, called "uid"
            ->set('data', $additional_claims)
            ->getToken($signer, $privateKey); // Retrieves the generated token


        $this->token = $token;
        
        return $token->__toString(); // The string representation of the object is a JWT string (pretty easy, right?)
    }

    public function toString() {
        return $this->token->__toString();;
    }

    public function getExpiration() {
        return $this->token->getClaim('exp');
    }
}
