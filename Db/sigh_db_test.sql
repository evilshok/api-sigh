-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 16-05-2021 a las 23:32:19
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sigh_db_test`
--
CREATE DATABASE IF NOT EXISTS `sigh_db_test` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE `sigh_db_test`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asesor`
--

DROP TABLE IF EXISTS `asesor`;
CREATE TABLE IF NOT EXISTS `asesor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tipo_usuario` int(11) NOT NULL,
  `id_oficina` int(11) DEFAULT 0,
  `id_rangos` int(11) DEFAULT 0,
  `permisos` text NOT NULL,
  `coleccion` text NOT NULL,
  `id_banco` int(11) NOT NULL,
  `id_constructora` int(11) NOT NULL,
  `id_desarrollo` int(11) NOT NULL,
  `id_inmobiliaria` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asesor`
--

INSERT INTO `asesor` (`id`, `nombre`, `correo`, `password`, `tipo_usuario`, `id_oficina`, `id_rangos`, `permisos`, `coleccion`, `id_banco`, `id_constructora`, `id_desarrollo`, `id_inmobiliaria`, `estatus`) VALUES
(1, 'test', 'test@test.com', '123456', 1, 0, 1, '1', 'sigh_db_test', 0, 0, 0, 0, 1),
(2, 'tadeo', 'zamora.figueroa@gmail.com', '25da5e13eb1fec21450fc837caa4582f', 8, 0, 0, '{\"lectura\":false,\"clientes\":true,\"catalogos\":false,\"registro_expediente\":false,\"seguimiento\":true,\"fases\":{\"analisis\":{\"consulta\":true,\"escritura\":true},\"autorizados\":{\"consulta\":true,\"escritura\":true},\"avaluo\":{\"consulta\":true,\"escritura\":true},\"instruccion\":{\"consulta\":true,\"escritura\":false},\"firma\":{\"consulta\":true,\"escritura\":false},\"fondeo\":{\"consulta\":true,\"escritura\":false},\"cobranza\":{\"consulta\":true,\"escritura\":false},\"comision\":{\"consulta\":true,\"escritura\":false}},\"comentarios\":false,\"leer_comentarios\":false}', '', 0, 0, 0, 0, 1),
(3, 'tadeo', 'tadeo@gmail.com', 'e23ee59cb916014b0ee53fcd9053ecf7', 2, 0, 0, '{\"lectura\":false,\"clientes\":true,\"catalogos\":false,\"registro_expediente\":false,\"seguimiento\":true,\"fases\":{\"analisis\":{\"consulta\":true,\"escritura\":true},\"autorizados\":{\"consulta\":true,\"escritura\":true},\"avaluo\":{\"consulta\":true,\"escritura\":true},\"instruccion\":{\"consulta\":true,\"escritura\":false},\"firma\":{\"consulta\":true,\"escritura\":false},\"fondeo\":{\"consulta\":true,\"escritura\":false},\"cobranza\":{\"consulta\":true,\"escritura\":false},\"comision\":{\"consulta\":true,\"escritura\":false}},\"comentarios\":false,\"leer_comentarios\":false}', 'sigh_db', 0, 0, 0, 0, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bancos`
--

DROP TABLE IF EXISTS `bancos`;
CREATE TABLE IF NOT EXISTS `bancos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_banco` varchar(255) DEFAULT NULL,
  `por_comision` decimal(10,2) NOT NULL DEFAULT 0.00,
  `sobremeta` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 AVG_ROW_LENGTH=1489 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `bancos`
--

INSERT INTO `bancos` (`id`, `nombre_banco`, `por_comision`, `sobremeta`) VALUES
(1, 'AFIRME', '1.70', 1),
(2, 'BANAMEX', '1.82', 1),
(3, 'BANREGIO', '1.82', 1),
(4, 'HSBC', '1.82', 1),
(5, 'SANTANDER', '1.82', 1),
(6, 'BANORTE', '1.82', 1),
(7, 'SCOTIABANK', '1.82', 1),
(8, 'HIPNAL', '1.40', 0),
(9, 'SOC', '0.00', 0),
(10, 'BX+', '1.82', 0),
(11, 'BIM', '0.00', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `bitacora_casos`
--

DROP TABLE IF EXISTS `bitacora_casos`;
CREATE TABLE IF NOT EXISTS `bitacora_casos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `operacion` text NOT NULL,
  `fecha` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `casos`
--

DROP TABLE IF EXISTS `casos`;
CREATE TABLE IF NOT EXISTS `casos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL COMMENT 'se refiere al id del usuario que lo esta registrando en esta tabla',
  `id_cliente_asesor` int(11) NOT NULL,
  `id_banco` int(11) NOT NULL,
  `id_esquema` int(11) NOT NULL,
  `id_esquema_compra` int(11) DEFAULT NULL,
  `id_apoyo` int(11) NOT NULL,
  `id_notaria` int(11) NOT NULL DEFAULT 0,
  `id_valuador` int(11) NOT NULL DEFAULT 0,
  `fecha_ingreso_banco` date NOT NULL,
  `fecha_riesgos` date DEFAULT NULL,
  `tasa` int(11) NOT NULL,
  `monto_solicitado` decimal(10,0) NOT NULL,
  `monto_autorizado` decimal(12,2) DEFAULT NULL,
  `monto_firmado` decimal(12,2) DEFAULT NULL,
  `ejecutivo_banco` text NOT NULL,
  `plazo` int(11) NOT NULL,
  `factor_pago` int(11) NOT NULL,
  `comision_apertura` int(11) NOT NULL,
  `pago_adelantado` int(11) NOT NULL,
  `folio_banco` text NOT NULL,
  `folio_soc` text NOT NULL,
  `incremento_factor_pago` int(11) NOT NULL,
  `id_fase` int(11) NOT NULL,
  `escrituras` text DEFAULT NULL,
  `fecha_ultima_fase` date NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1,
  `razon` text DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `casos`
--

INSERT INTO `casos` (`id`, `id_usuario`, `id_cliente_asesor`, `id_banco`, `id_esquema`, `id_esquema_compra`, `id_apoyo`, `id_notaria`, `id_valuador`, `fecha_ingreso_banco`, `fecha_riesgos`, `tasa`, `monto_solicitado`, `monto_autorizado`, `monto_firmado`, `ejecutivo_banco`, `plazo`, `factor_pago`, `comision_apertura`, `pago_adelantado`, `folio_banco`, `folio_soc`, `incremento_factor_pago`, `id_fase`, `escrituras`, `fecha_ultima_fase`, `activo`, `razon`) VALUES
(1, 1, 1, 2, 89, NULL, 1, 1, 1, '2021-03-03', NULL, 10, '1800000', '1500000.00', NULL, 'Juan', 20, 12, 1500, 1200, '1234', '12345', 12, 3, NULL, '2021-03-02', 1, NULL),
(2, 1, 1, 1, 2, NULL, 2, 1, 1, '2021-03-02', '2021-03-31', 10, '1500000', '1500000.00', '1850000.00', 'pepe', 20, 20, 20, 20, '12344', '123445', 20, 9, '123456', '2021-03-03', 1, NULL),
(3, 1, 1, 1, 1, NULL, 1, 1, 1, '2021-03-03', NULL, 10, '1200000', '1800000.00', NULL, 'Juan perez', 20, 20, 20, 20, '999', '999', 20, 3, NULL, '2021-03-03', 0, 'por prueba');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cat_fases`
--

DROP TABLE IF EXISTS `cat_fases`;
CREATE TABLE IF NOT EXISTS `cat_fases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fase` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 AVG_ROW_LENGTH=1638 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cat_fases`
--

INSERT INTO `cat_fases` (`id`, `fase`) VALUES
(1, 'Analisis'),
(2, 'Autorizado'),
(3, 'Avaluo'),
(4, 'Instruccion'),
(5, 'Firma'),
(6, 'Fondeo'),
(7, 'Cobranza'),
(8, 'Comision'),
(9, 'Pago');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `clientes`
--

DROP TABLE IF EXISTS `clientes`;
CREATE TABLE IF NOT EXISTS `clientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `apellido_paterno` varchar(50) NOT NULL,
  `apellido_materno` varchar(50) NOT NULL,
  `curp` text NOT NULL,
  `correo` varchar(50) NOT NULL,
  `telefono` text NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `fecha_ingreso` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `estatus` int(11) NOT NULL DEFAULT 1,
  `ingreso_mensual` decimal(10,0) NOT NULL,
  `tipo_economia` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `clientes`
--

INSERT INTO `clientes` (`id`, `nombre`, `apellido_paterno`, `apellido_materno`, `curp`, `correo`, `telefono`, `fecha_nacimiento`, `fecha_ingreso`, `estatus`, `ingreso_mensual`, `tipo_economia`) VALUES
(1, 'tadeo fernando', 'zamora', 'figueroa', 'zaft910912hsrmgd07', 'zamora.figueroa@gmail.com', '6622977542', '1991-09-12', '2021-02-23 17:49:40', 1, '25000', 'Asalariado'),
(2, 'test', 'zamora', 'figueroa', 'zaft910912hsrmgd06', 'zamora.figueroa@gmail.com', '6622977542', '1991-09-12', '2021-02-23 17:49:44', 1, '25000', 'Asalariado'),
(3, 'test2', 'zamora', 'figueroa', 'zaft910912hsrmgd08', 'zamora.figueroa@gmail.com', '6622977542', '1991-09-12', '2021-02-23 17:49:48', 1, '25000', 'Asalariado'),
(4, 'test3', 'zamora', 'figueroa', 'zaft910912hsrmgd09', 'zamora.figueroa@gmail.com', '6622977542', '1991-09-12', '2021-02-23 17:49:51', 1, '25000', 'Asalariado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente_asesor`
--

DROP TABLE IF EXISTS `cliente_asesor`;
CREATE TABLE IF NOT EXISTS `cliente_asesor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_asesor` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT 1 COMMENT 'Nos ayuda a saber si ya se cerro expediente 1 o 0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `cliente_asesor`
--

INSERT INTO `cliente_asesor` (`id`, `id_cliente`, `id_asesor`, `activo`) VALUES
(1, 1, 1, 1),
(2, 2, 1, 1),
(3, 3, 1, 1),
(4, 4, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentarios`
--

DROP TABLE IF EXISTS `comentarios`;
CREATE TABLE IF NOT EXISTS `comentarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_caso` int(11) NOT NULL,
  `comentario` text NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `id_fase` int(11) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp(),
  `estatus` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `comentarios`
--

INSERT INTO `comentarios` (`id`, `id_caso`, `comentario`, `usuario`, `id_usuario`, `id_fase`, `fecha`, `estatus`) VALUES
(1, 2, 'prueba', 'Tadeo', 1, 1, '2021-03-06 20:46:28', 1),
(2, 2, 'prueba 2', 'Tadeo', 1, 1, '2021-03-07 02:32:32', 1),
(3, 2, 'que mas te puedo decir', 'Tadeo', 1, 1, '2021-03-07 02:32:41', 1),
(4, 2, 'no se dimelo tu\njaajajja', 'Tadeo', 1, 1, '2021-03-07 02:33:04', 1),
(5, 2, 'jjajajajaj', 'Tadeo', 1, 1, '2021-03-07 02:45:59', 1),
(6, 2, 'prueba en autorizados', 'Tadeo', 1, 2, '2021-03-11 17:44:08', 1),
(7, 2, 'prueba en autorizados 22', 'Tadeo', 1, 2, '2021-03-11 18:08:39', 1),
(8, 1, 'primmer comentario', 'Tadeo', 1, 2, '2021-03-22 21:13:41', 1),
(9, 1, 'otro nuevo', 'Tadeo', 1, 2, '2021-03-22 21:33:02', 1),
(10, 3, 'primero', 'Tadeo', 1, 3, '2021-03-22 21:33:11', 1),
(11, 1, 'comentario', 'Tadeo', 1, 2, '2021-03-24 17:19:14', 1),
(12, 1, 'asdad', 'Tadeo', 1, 3, '2021-04-08 23:23:10', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `constructoras`
--

DROP TABLE IF EXISTS `constructoras`;
CREATE TABLE IF NOT EXISTS `constructoras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_constructora` varchar(255) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 AVG_ROW_LENGTH=862 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `constructoras`
--

INSERT INTO `constructoras` (`id`, `nombre_constructora`, `estatus`) VALUES
(1, 'PROVIDA', 1),
(2, 'RUBA', 1),
(3, 'ASISTENCIA ADMINISTRATIVA', 1),
(4, 'PROMOTORA DE HOGARES', 1),
(5, 'MERCADO ABIERTO', 1),
(6, 'CENTURY ELGA', 1),
(7, 'OUR OASIS ', 1),
(8, 'ESPACIO DU ', 1),
(9, 'CAPISTRANO', 1),
(10, 'ECO', 1),
(11, 'VERTEX', 1),
(12, 'VILLA CALIFORNIA', 1),
(13, 'RETI  SA PI SA DE CV', 1),
(14, 'PROVINCIAS', 1),
(15, 'OASIS', 1),
(16, 'DEREX', 1),
(17, 'STANZA', 1),
(18, 'LA PRUEBA', 1),
(19, '', 1),
(20, 'Milenium', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `desarrollo`
--

DROP TABLE IF EXISTS `desarrollo`;
CREATE TABLE IF NOT EXISTS `desarrollo` (
  `id_desarrollo` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_desarrollo` varchar(255) DEFAULT NULL,
  `id_constructora` int(11) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id_desarrollo`)
) ENGINE=InnoDB AUTO_INCREMENT=49 AVG_ROW_LENGTH=481 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `desarrollo`
--

INSERT INTO `desarrollo` (`id_desarrollo`, `nombre_desarrollo`, `id_constructora`, `estatus`) VALUES
(1, 'LA ENCANTADA', 1, 1),
(2, 'LA CIMA', 2, 1),
(3, 'BELMONTE', 3, 1),
(4, 'CORCELES', 4, 1),
(5, 'ALTA FIRENZE', 4, 1),
(6, 'PITIC', 5, 1),
(7, 'MONTEROSA', 6, 1),
(8, 'SANTA BARBARA', 6, 1),
(9, 'CEZANE', 7, 1),
(10, 'PASEO DEL CID', 8, 1),
(11, 'LOS JARDINES', 5, 1),
(12, 'VILLA FONTANA', 5, 1),
(13, 'SAN JUAN CAPISTRANO', 9, 1),
(14, 'LOMA LINDA', 5, 1),
(15, 'SANTERRA', 10, 1),
(16, 'LA RIOJA', 11, 1),
(17, 'LOMAS DEL PARAISO', 12, 1),
(18, 'RDCIAL LA CANTERA', 13, 1),
(19, 'RIVELO', 4, 1),
(20, 'LAGO ESCONDIDO', 5, 1),
(22, 'SOLEIL RESIDENCIAL', 7, 1),
(23, 'CASTELLO RESIDENCIAL', 4, 1),
(24, '', 14, 1),
(25, 'VILLA MERLOT', 4, 1),
(26, 'CEZANNE', 15, 1),
(27, 'CARDENO', 6, 1),
(28, 'SIENA', 16, 1),
(29, 'ALONDRAS', 16, 1),
(30, 'BARCELO', 16, 1),
(31, 'BOSCO', 16, 1),
(32, 'SOLARE', 17, 1),
(33, 'D PRUEBA', 18, 1),
(40, 'HLS', 5, 1),
(41, 'HLS', 5, 1),
(42, 'FLORENZA', 17, 1),
(43, 'FLORENZA', 17, 1),
(44, 'TORRALBA', 17, 1),
(45, 'Real de Castilla', 20, 1),
(46, 'Real de Sevilla', 20, 1),
(47, 'Real de Toledo', 20, 1),
(48, 'Salamanca', 20, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esquemas`
--

DROP TABLE IF EXISTS `esquemas`;
CREATE TABLE IF NOT EXISTS `esquemas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_esquema` varchar(255) DEFAULT NULL,
  `id_banco` int(11) DEFAULT NULL,
  `estatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 AVG_ROW_LENGTH=176 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `esquemas`
--

INSERT INTO `esquemas` (`id`, `nombre_esquema`, `id_banco`, `estatus`) VALUES
(1, 'FIJO 9.80', 1, 0),
(2, 'FIJO 10.55', 1, 0),
(3, 'CRECIENTE 9.80', 1, 0),
(4, 'CRECIENTE 10.55', 1, 0),
(5, 'HIPOTECA PERFILES', 2, 1),
(6, 'TERRENO TASA FIJA', 3, 0),
(7, 'PAGO BAJO', 4, 1),
(8, 'PAGO FIJO', 4, 1),
(9, 'HIPOTECA SELECT CONGELADO', 5, 0),
(10, 'HIPOTECA SELECT CRECIENTE', 5, 0),
(11, 'HIPOTECA PREMIER CONGELADOS', 5, 0),
(12, 'HIPOTECA PREMIER CRECIENTES', 5, 0),
(13, 'HIPOTECA INTELIGENTE', 5, 0),
(14, 'HIPOTECA ELITE', 6, 1),
(15, 'HIPOTECA FLEXIBLE', 6, 1),
(16, 'HIPOTECA MAS X MENOS', 6, 1),
(17, 'HIPOTECA ACCESIBLE', 6, 0),
(18, 'HIPOTECA FIJA BANORTE', 6, 0),
(19, 'PESOS TASA FIJA MENOR ENGANCHE', 8, 1),
(20, 'PESOS TASA FIJA MENOR TASA', 8, 1),
(21, 'HIPOTECA JOVEN MENOR ENGANCHE', 8, 1),
(22, 'HIPOTECA JOVEN MENOR TASA', 8, 1),
(23, 'HIPOTECA RESIDENCIAL', 8, 1),
(24, 'HIPOTECA JOVEN RESIDENCIAL', 8, 1),
(25, 'VALORA', 7, 1),
(26, 'PAGOS OPORTUNOS', 7, 1),
(27, '7x5', 7, 1),
(29, 'ACCESIBLE', NULL, 0),
(30, 'BANREGIO', NULL, 0),
(31, 'ELITE', NULL, 0),
(32, 'HIPOTECA 10 X 1000', 5, 0),
(33, 'HIPOTECA JOVEN', NULL, 0),
(36, 'HIPOTECA RESIDENCIAL', NULL, 0),
(37, 'INTELIGENTE', NULL, 0),
(38, 'LIGHT PAGOS CRECIENTES', 5, 0),
(43, 'PESOS', 8, 0),
(44, 'PESOS FIJOS', 8, 0),
(45, 'PESOS FIJOS MENOR TASA', 8, 0),
(46, 'PESOS TASA FIJA', 8, 0),
(47, 'PESOS TASA FIJA ENGANCHE BAJO', NULL, 0),
(48, 'PREMIER', 5, 0),
(49, 'SELECT', 5, 0),
(50, 'TASA FIJA', 1, 0),
(51, 'TRADICIONAL PESOS', 1, 0),
(52, 'PAGO FIJO', 8, 0),
(53, 'PAGO FIJO', 6, 0),
(54, 'TASA FIJA', 2, 0),
(55, 'TASA FIJA', 6, 0),
(56, 'tasa 8.99', 6, 0),
(57, 'PAGO DE PASIVOS', 7, 0),
(58, 'TASA 8.24', 4, 0),
(59, 'PAGO HIPOTECA PAGO BAJO', 4, 0),
(60, '7X5', NULL, NULL),
(61, 'ACCESIBLE', NULL, NULL),
(62, 'BANREGIO', NULL, NULL),
(63, 'ELITE', NULL, NULL),
(64, 'HIPOTECA 10 X 1000', NULL, NULL),
(65, 'HIPOTECA JOVEN', NULL, NULL),
(66, 'HIPOTECA JOVEN MENOR ENGANCHE', NULL, NULL),
(67, 'HIPOTECA JOVEN RESIDENCIAL', NULL, NULL),
(68, 'HIPOTECA MAS X MENOS', NULL, NULL),
(69, 'HIPOTECA RESIDENCIAL', NULL, NULL),
(70, 'INTELIGENTE', NULL, NULL),
(71, 'LIGHT PAGOS CRECIENTES', NULL, 0),
(72, 'PAGO BAJO', NULL, NULL),
(73, 'PAGO FIJO', NULL, NULL),
(74, 'PAGOS OPORTUNOS', NULL, NULL),
(75, 'PESOS', NULL, NULL),
(76, 'PESOS FIJOS', NULL, NULL),
(77, 'PESOS FIJOS MENOR TASA', NULL, NULL),
(78, 'PESOS TASA FIJA', NULL, NULL),
(79, 'PESOS TASA FIJA ENGANCHE BAJO', NULL, NULL),
(80, 'PREMIER', NULL, 0),
(81, 'SELECT', NULL, NULL),
(82, 'TASA FIJA', NULL, NULL),
(83, 'TRADICIONAL PESOS', NULL, NULL),
(84, 'VALORA', NULL, NULL),
(85, 'PRODUCTO PAGO DE PASIVOS', 7, 0),
(86, 'SUSTITUCION DE HIPOTECA', 5, 1),
(87, 'ADQUISICIÓN', 3, 0),
(88, 'CONSTRUCCIÓN', 7, 0),
(89, 'CAMBIA TU HIPOTECA', 2, 1),
(90, 'LIQUIDEZ', 8, 1),
(91, 'MEJORA HIPOTECA', 10, 1),
(92, 'MAS LIQUIDEZ', 10, 1),
(93, 'ADQUISICIÓN', 10, 1),
(94, 'INFONAVIT', 10, 1),
(95, 'COFINAVIT', 10, 1),
(96, 'LIQUIDEZ', 10, 1),
(97, 'BIM ADQUISICION', 11, 1),
(98, 'Hipoteca Personal', 5, 1),
(99, 'CONSTRUCCION', 6, 0),
(100, 'Hipoteca Fuerte', 6, 1),
(102, 'PROGRAMADOS', 5, 0),
(103, 'CRECIENTE', 1, 1),
(104, 'ELITE', 1, 1),
(106, 'TRADICIONAL', 3, 1),
(107, '1+1', 3, 1),
(108, 'FLEX', 3, 1),
(109, 'VERTICAL', 3, 1),
(110, 'FIJO', 3, 1),
(111, 'CRECIENTE', 3, 1),
(112, 'Hipoteca Santander Fijo', 5, 1),
(113, 'Hipoteca Santander Creciente', 5, 1),
(114, 'Hipoteca Free Fijo', 5, 1),
(115, 'Hipoteca Free Creciente', 5, 1),
(116, 'Super Casa', 5, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `esquema_compra`
--

DROP TABLE IF EXISTS `esquema_compra`;
CREATE TABLE IF NOT EXISTS `esquema_compra` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente_asesor` int(11) NOT NULL,
  `id_constructora` int(11) NOT NULL,
  `id_desarrollo` int(11) NOT NULL,
  `id_inmobiliaria` int(11) NOT NULL,
  `valor_vivienda` decimal(10,0) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `esquema_compra`
--

INSERT INTO `esquema_compra` (`id`, `id_cliente_asesor`, `id_constructora`, `id_desarrollo`, `id_inmobiliaria`, `valor_vivienda`, `estatus`) VALUES
(1, 1, 0, 0, 0, '1200000', 0),
(2, 3, 0, 0, 0, '1200000', 1),
(3, 4, 0, 0, 0, '1200000', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `fecha_fases`
--

DROP TABLE IF EXISTS `fecha_fases`;
CREATE TABLE IF NOT EXISTS `fecha_fases` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_caso` varchar(255) DEFAULT NULL,
  `vencimiento` date DEFAULT NULL,
  `analisis` date DEFAULT NULL,
  `autorizado` date DEFAULT NULL,
  `avaluo` date DEFAULT NULL,
  `instruccion` date DEFAULT NULL,
  `firma` date DEFAULT NULL,
  `fondeo` date DEFAULT NULL,
  `cobranza` date DEFAULT NULL,
  `comision` date DEFAULT NULL,
  `pago` date DEFAULT NULL,
  `firmado` int(11) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 AVG_ROW_LENGTH=8192 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `fecha_fases`
--

INSERT INTO `fecha_fases` (`id`, `id_caso`, `vencimiento`, `analisis`, `autorizado`, `avaluo`, `instruccion`, `firma`, `fondeo`, `cobranza`, `comision`, `pago`, `firmado`) VALUES
(1, '1', '2021-03-10', '2021-03-03', '2021-03-10', '2021-03-24', NULL, NULL, NULL, NULL, NULL, NULL, 0),
(2, '2', '2021-03-10', '2021-03-02', '2021-03-10', '2021-03-30', '2021-03-12', '2021-05-01', '2021-05-03', '2021-06-23', '2021-08-31', '2021-11-30', 1),
(3, '3', '2021-06-18', '2021-03-03', '2021-03-10', '2021-03-12', NULL, NULL, NULL, NULL, NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gobierno`
--

DROP TABLE IF EXISTS `gobierno`;
CREATE TABLE IF NOT EXISTS `gobierno` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_apoyo` varchar(255) DEFAULT NULL,
  `estatus` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 AVG_ROW_LENGTH=4096 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `gobierno`
--

INSERT INTO `gobierno` (`id`, `nombre_apoyo`, `estatus`) VALUES
(1, 'ALIADOS', 1),
(2, 'RESPALDADOS', 1),
(3, 'COFINAVIT', 1),
(4, 'APOYO', 1),
(5, 'PYME', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notarias`
--

DROP TABLE IF EXISTS `notarias`;
CREATE TABLE IF NOT EXISTS `notarias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `notaria` int(11) DEFAULT NULL,
  `nombre_notaria` varchar(255) DEFAULT NULL,
  `ciudad_notaria` varchar(255) DEFAULT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 AVG_ROW_LENGTH=420 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `notarias`
--

INSERT INTO `notarias` (`id`, `notaria`, `nombre_notaria`, `ciudad_notaria`, `estatus`) VALUES
(1, 1, 'Tadeo', 'HMO', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valuador`
--

DROP TABLE IF EXISTS `valuador`;
CREATE TABLE IF NOT EXISTS `valuador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_valuador` varchar(70) DEFAULT NULL,
  `correo` varchar(50) DEFAULT NULL,
  `telefono` varchar(30) DEFAULT NULL,
  `id_unidad` int(11) NOT NULL,
  `activo` int(11) DEFAULT 1,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 AVG_ROW_LENGTH=564 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `valuador`
--

INSERT INTO `valuador` (`id`, `nombre_valuador`, `correo`, `telefono`, `id_unidad`, `activo`) VALUES
(1, 'tadeo zamora', 'zamora.figueroa@gmail.com', '6622977542', 1, 1);

-- --------------------------------------------------------

--
-- Estructura Stand-in para la vista `v_expediente`
-- (Véase abajo para la vista actual)
--
DROP VIEW IF EXISTS `v_expediente`;
CREATE TABLE IF NOT EXISTS `v_expediente` (
`id` int(11)
,`id_cliente_asesor` int(11)
,`fecha_ingreso_banco` date
,`monto_solicitado` decimal(10,0)
,`monto_autorizado` decimal(12,2)
,`monto_firmado` decimal(12,2)
,`vencimiento` date
,`fecha_riesgos` date
,`nombre_banco` varchar(255)
,`nombre_esquema` varchar(255)
,`folio_banco` text
,`folio_soc` text
,`fase` varchar(255)
,`activo` int(11)
,`razon` text
,`id_cliente` int(11)
,`id_asesor` int(11)
,`cliente` varchar(152)
);

-- --------------------------------------------------------

--
-- Estructura para la vista `v_expediente`
--
DROP TABLE IF EXISTS `v_expediente`;

DROP VIEW IF EXISTS `v_expediente`;
CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_expediente`  AS SELECT `c`.`id` AS `id`, `c`.`id_cliente_asesor` AS `id_cliente_asesor`, `c`.`fecha_ingreso_banco` AS `fecha_ingreso_banco`, `c`.`monto_solicitado` AS `monto_solicitado`, `c`.`monto_autorizado` AS `monto_autorizado`, `c`.`monto_firmado` AS `monto_firmado`, `ff`.`vencimiento` AS `vencimiento`, `c`.`fecha_riesgos` AS `fecha_riesgos`, `b`.`nombre_banco` AS `nombre_banco`, `e`.`nombre_esquema` AS `nombre_esquema`, `c`.`folio_banco` AS `folio_banco`, `c`.`folio_soc` AS `folio_soc`, `cf`.`fase` AS `fase`, `c`.`activo` AS `activo`, `c`.`razon` AS `razon`, `ca`.`id_cliente` AS `id_cliente`, `ca`.`id_asesor` AS `id_asesor`, concat_ws(' ',`cl`.`nombre`,`cl`.`apellido_paterno`,`cl`.`apellido_materno`) AS `cliente` FROM ((((((`casos` `c` join `fecha_fases` `ff` on(`c`.`id` = `ff`.`id_caso`)) join `cliente_asesor` `ca` on(`c`.`id_cliente_asesor` = `ca`.`id`)) join `clientes` `cl` on(`ca`.`id_cliente` = `cl`.`id`)) join `bancos` `b` on(`c`.`id_banco` = `b`.`id`)) join `esquemas` `e` on(`c`.`id_esquema` = `e`.`id`)) join `cat_fases` `cf` on(`c`.`id_fase` = `cf`.`id`)) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
