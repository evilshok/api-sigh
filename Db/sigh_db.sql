-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 16-05-2021 a las 23:28:29
-- Versión del servidor: 10.4.14-MariaDB
-- Versión de PHP: 7.2.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sigh_db`
--
CREATE DATABASE IF NOT EXISTS `sigh_db` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `sigh_db`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `asesor`
--

DROP TABLE IF EXISTS `asesor`;
CREATE TABLE IF NOT EXISTS `asesor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `tipo_usuario` int(11) NOT NULL,
  `id_oficina` int(11) DEFAULT 0,
  `id_rangos` int(11) DEFAULT 0,
  `permisos` text NOT NULL,
  `coleccion` text NOT NULL,
  `id_banco` int(11) NOT NULL,
  `id_constructora` int(11) NOT NULL,
  `id_desarrollo` int(11) NOT NULL,
  `id_inmobiliaria` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `asesor`
--

INSERT INTO `asesor` (`id`, `nombre`, `correo`, `password`, `tipo_usuario`, `id_oficina`, `id_rangos`, `permisos`, `coleccion`, `id_banco`, `id_constructora`, `id_desarrollo`, `id_inmobiliaria`) VALUES
(1, 'test', 'test@test.com', '123456', 1, 0, 1, '1', 'sigh_db_test', 0, 0, 0, 0),
(2, 'tadeo', 'zamora.figueroa@gmail.com', '25da5e13eb1fec21450fc837caa4582f', 2, 0, 0, '{\"lectura\":false,\"clientes\":true,\"catalogos\":false,\"registro_expediente\":false,\"seguimiento\":true,\"fases\":{\"analisis\":{\"consulta\":true,\"escritura\":true},\"autorizados\":{\"consulta\":true,\"escritura\":true},\"avaluo\":{\"consulta\":true,\"escritura\":true},\"instruccion\":{\"consulta\":true,\"escritura\":false},\"firma\":{\"consulta\":true,\"escritura\":false},\"fondeo\":{\"consulta\":true,\"escritura\":false},\"cobranza\":{\"consulta\":true,\"escritura\":false},\"comision\":{\"consulta\":true,\"escritura\":false}},\"comentarios\":false,\"leer_comentarios\":false}', '', 0, 0, 0, 0),
(3, 'tadeo', 'tadeo@gmail.com', 'e23ee59cb916014b0ee53fcd9053ecf7', 2, 0, 0, '{\"lectura\":false,\"clientes\":true,\"catalogos\":false,\"registro_expediente\":false,\"seguimiento\":true,\"fases\":{\"analisis\":{\"consulta\":true,\"escritura\":true},\"autorizados\":{\"consulta\":true,\"escritura\":true},\"avaluo\":{\"consulta\":true,\"escritura\":true},\"instruccion\":{\"consulta\":true,\"escritura\":false},\"firma\":{\"consulta\":true,\"escritura\":false},\"fondeo\":{\"consulta\":true,\"escritura\":false},\"cobranza\":{\"consulta\":true,\"escritura\":false},\"comision\":{\"consulta\":true,\"escritura\":false}},\"comentarios\":false,\"leer_comentarios\":false}', 'sigh_db', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empresa`
--

DROP TABLE IF EXISTS `empresa`;
CREATE TABLE IF NOT EXISTS `empresa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` int(11) NOT NULL,
  `coleccion` text NOT NULL,
  `direccion` int(11) NOT NULL,
  `telefonos` int(11) NOT NULL,
  `estatus` int(11) NOT NULL DEFAULT 1,
  `logo` varchar(255) NOT NULL,
  `creado` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
